import React from 'react';
import ApartmentsList from "./containers/apartments-list";

export default function App() {
    return (
        <div className="container">
            <ApartmentsList/>
        </div>
    );
};
