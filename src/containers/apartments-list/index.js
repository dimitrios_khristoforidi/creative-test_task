import React, {useEffect, useState} from "react"
import API from "../../API";
import ApartmentCard from "../../components/apartment-card";

export default function ApartmentsList() {
    const [apartmentsList, setApartmentsList] = useState([])

    useEffect(() => {
        API.getApartmentsList()
            .then(r => setApartmentsList(r.response))
            .catch(e => console.log(e))
    }, [])

    return (
        <div className="grid-3">
            {
                apartmentsList && apartmentsList.map((item, id) => (
                    <ApartmentCard key={id} apartmentData={item}/>
                ))
            }
        </div>
    )
}