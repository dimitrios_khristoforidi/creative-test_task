import React from "react"

export default function Heart() {
    return (
        <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <g clipPath="url(#clip0)">
                <path
                    d="M18.0586 2.44531C15.918 0.621094 12.7344 0.949219 10.7695 2.97656L10 3.76953L9.23048 2.97656C7.26954 0.949219 4.08204 0.621094 1.94141 2.44531C-0.511713 4.53906 -0.640619 8.29688 1.55469 10.5664L9.11329 18.3711C9.60157 18.875 10.3945 18.875 10.8828 18.3711L18.4414 10.5664C20.6406 8.29688 20.5117 4.53906 18.0586 2.44531V2.44531Z"
                    fill="#888888"/>
            </g>
            <defs>
                <clipPath id="clip0">
                    <rect width="20" height="20" fill="white"/>
                </clipPath>
            </defs>
        </svg>
    )
}