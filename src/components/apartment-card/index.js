import React from "react"
import "./index.css"
import Flat from "../../assets/icons/Flat";
import House from "../../assets/icons/House";
import ApartmentsPlug from "../../assets/images/apartments-plug.png";
import {connect} from "react-redux";
import {addLike, removeLike} from "../../redux/actions";
import Heart from "../../assets/icons/Heart";

function ApartmentCard({apartmentData, removeLike, addLike, likedApartments}) {
    const apartmentsIcons = {
        "flat": <Flat/>,
        "house": <House/>,
    }

    const generateAddress = (city, street, house, room) => {
        return `${city}
        ${street}
        ${house}/${room}`
    }

    const handleLike = (id, checked) => {
        if (checked) addLike(id)
        else removeLike(id)
    }

    if (apartmentData)
        return (
            <div className="apartment-card">
                <div className="apartment-card__icon flex a-center j-center">
                    {apartmentsIcons[apartmentData.type]}
                </div>
                <div className="w-100 h-100 flex-col a-center j-between">
                    <div className="apartment-card__header w-100">
                        <span className="apartment-card__image-cover w-100 h-100"/>
                        <img className="apartment-card__image w-100" alt="image" src={ApartmentsPlug}/>
                        <label className="apartment-card__header__button flex a-center j-center">
                            <input type="checkbox" checked={likedApartments.indexOf(apartmentData.id) !== -1}
                                   onChange={(e) => handleLike(apartmentData.id, e.target.checked)}/>
                            <Heart/>
                        </label>
                    </div>
                    <div className="apartment-card__main w-100 flex-col a-center j-end">
                        <h3>{apartmentData.attributes.title}</h3>
                        <h4>{generateAddress(apartmentData.attributes.address.city, apartmentData.attributes.address.street, apartmentData.attributes.address.house, apartmentData.attributes.address.room)}</h4>
                    </div>
                    <div className="apartment-card__footer w-100 flex a-center j-between">
                        <div className="apartment-card__footer-item flex-col a-center j-start">
                            <h5>{apartmentData.attributes.rooms}</h5>
                            <p>комнат</p>
                        </div>
                        <span className="apartment-card__footer-item--hr"/>
                        <div className="apartment-card__footer-item flex-col a-center j-start">
                            <h5>{apartmentData.attributes.area}</h5>
                            <p>{apartmentData.attributes.unit}</p>
                        </div>
                        <span className="apartment-card__footer-item--hr"/>
                        <div className="apartment-card__footer-item flex-col a-center j-start">
                            <h5>{apartmentData.relationships.attributes.last_name + " " + apartmentData.relationships.attributes.first_name}</h5>
                            <h5>{apartmentData.relationships.attributes.middle_name}</h5>
                        </div>
                    </div>
                </div>
            </div>
        )
    else
        return ("")
}

const mapDispatchToProps = {
    addLike,
    removeLike
}

const mapStateToProp = state => ({
    likedApartments: state.apartments.likes
})

export default connect(mapStateToProp, mapDispatchToProps)(ApartmentCard)