import React from 'react';
import {render} from 'react-dom';
import './index.css';
import App from "./App";
import {Provider} from "react-redux";
import {PersistGate} from 'redux-persist/integration/react'
import configureStore from "./redux/configureStore";

const {store, persistor} = configureStore()

const APP =
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <App/>
        </PersistGate>
    </Provider>

render(APP, document.getElementById('root'));