import {ADD_LIKE} from "../types";
import {REMOVE_LIKE} from "../types";

const initialState = {
    likes: []
}

export const apartmentsReducer = (state = initialState, action) => {
    switch (action.type) {
        case ADD_LIKE:
            return {...state, likes: [...state.likes, action.payload]}
        case REMOVE_LIKE:
            let newLikesState = state.likes.filter(item => item !== action.payload)
            return {...state, likes: newLikesState}
        default :
            return state
    }
}