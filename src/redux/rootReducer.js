import {combineReducers} from "redux";
import {apartmentsReducer} from "./reducers/apartmentsReducer";

const rootReducer = combineReducers({
    apartments: apartmentsReducer
})

export default rootReducer