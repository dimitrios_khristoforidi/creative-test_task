import {ADD_LIKE} from "./types";
import {REMOVE_LIKE} from "./types";

export function addLike(id) {
    return {
        type: ADD_LIKE,
        payload: id,
    }
}

export function removeLike(id) {
    return {
        type: REMOVE_LIKE,
        payload: id,
    }
}